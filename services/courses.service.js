const mongoose = require("mongoose")
const express = require('express')
require("dotenv").config();

const SSLCommerzPayment = require('sslcommerz-lts')
const store_id = process.env.STORE_ID;
const store_passwd = process.env.STORE_PASS;
const is_live = false //true for live, false for sandbox

const Courses = require('../models/Courses');
const CourseCategories = require("../models/CourseCategories")
const PurchaseCourse = require('../models/PurchaseCourse')
const CourseContents = require("../models/CourseContents")

//add course
exports.createCourseService = async (data) => {
    const Course = await Courses.create(data);
    return Course;
}

//all course get api
exports.getCoursesService = async () => {
    const courseAll = await Courses.find({})
    return courseAll;
}

exports.getCourseByIdService = async (courseId) => {
    const courseById = await Courses.findOne({ _id: courseId })
    return courseById;
}

exports.getSpecificCourseServiec = async (name) => {
    const specificCourse = await Courses.find({ courseCategories: name })
    return specificCourse;
}

exports.createCourseCategoriesService = async (data) => {
    const CourseCategorie = await CourseCategories.create(data);
    return CourseCategorie;
}

exports.getCourseCategoriesService = async () => {
    const CourseCategorieGet = await CourseCategories.find({});
    return CourseCategorieGet;
}

exports.deleteCategoriesByIdService = async (id) => {
    const result = await CourseCategories.deleteOne({ _id: id })
    return result;
}
exports.deleteCourseByIdService = async (id) => {
    const result = await Courses.deleteOne({ _id: id })
    return result;
}

exports.updateCategoriesByIdService = async (categoriesId, data) => {
    const result = await CourseCategories.updateOne({ _id: categoriesId }, { $set: data }, {
        runValidators: true
    });
    return result;
}

exports.getCategoriesByIdService = async (id) => {
    const result = await CourseCategories.findOne({ _id: id })
    return result;
}

exports.postPymentByInfoService = async (courseData) => {
    const orderedCourse = await Courses.findOne({ _id: courseData.courseId })
    const courseContentFind = await CourseContents.find({ courseName: courseData.courseName })
    const transactionId = new mongoose.Types.ObjectId().toString()
    const data = {
        total_amount: orderedCourse.price,
        currency: courseData.currency,
        tran_id: transactionId, // use unique tran_id for each api call
        success_url: `${process.env.SERVER_URL}/api/v1/courses/payment/success?transactionId=${transactionId}`,
        fail_url: `${process.env.SERVER_URL}/api/v1/courses/payment/fail?transactionId=${transactionId}`,
        cancel_url: `${process.env.SERVER_URL}/api/v1/courses/payment/cancel?transactionId=${transactionId}`,
        ipn_url: `http://localhost:3030/ipn`,
        shipping_method: 'Online',
        product_name: orderedCourse.title,
        product_category: orderedCourse.courseCategories,
        product_profile: 'general',
        cus_name: courseData.name,
        cus_email: courseData.email,
        cus_add1: courseData.address,
        cus_add2: 'Dhaka',
        cus_city: 'Dhaka',
        cus_state: 'Dhaka',
        cus_postcode: courseData.postcode,
        cus_country: 'Bangladesh',
        cus_phone: courseData.phoneNumber,
        cus_fax: '01711111111',
        ship_name: 'Customer Name',
        ship_add1: 'Dhaka',
        ship_add2: 'Dhaka',
        ship_city: 'Dhaka',
        ship_state: 'Dhaka',
        ship_postcode: courseData.postcode,
        ship_country: 'Bangladesh',
    };
    const sslcz = new SSLCommerzPayment(store_id, store_passwd, is_live)
    const ssl = sslcz.init(data).then(apiResponse => {
        // Redirect the user to payment gateway
        let GatewayPageURL = apiResponse.GatewayPageURL
        PurchaseCourse.create({
            ...courseData,
            transactionId,
            paid: false,
            courseImg: orderedCourse.img,
            courseContent: courseContentFind

        });
        return GatewayPageURL
    });
    return ssl;
}

exports.paymentSuccessService = async (transactionId) => {
    const result = await PurchaseCourse.updateOne({ transactionId }, { $set: { paid: true, paidAt: new Date() } })
    return result;
}
exports.paymentFailService = async (transactionId) => {
    const result = await PurchaseCourse.deleteOne({ transactionId })
    return result;
}
exports.paymentCancelService = async (transactionId) => {
    const result = await PurchaseCourse.deleteOne({ transactionId })
    return result;
}
exports.purchaseCourseService = async (id) => {
    const result = await PurchaseCourse.findOne({ transactionId: id })
    return result;
}

exports.getMyCourseByEmailService = async (email) => {
    const result = await PurchaseCourse.find({ email: email, paid: true })
    return result;
}
