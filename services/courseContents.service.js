const CourseContents = require("../models/CourseContents")
const Month = require("../models/ContentMonth")
const Subject = require("../models/SubjectAdd")

exports.createCourseContentService = async (data) => {
    const result = await CourseContents.create(data);
    return result;
}
exports.getCourseContentService = async () => {
    const result = await CourseContents.find({});
    return result;
}
exports.getCourseContentByNameService = async (courseName) => {
    const result = await CourseContents.find({ courseName: courseName });
    return result;
}
exports.getCourseContentByIdService = async (id) => {
    const result = await CourseContents.findOne({ _id: id });
    return result;
}
exports.getCourseContentClassByIdService = async (courseId, classname) => {
    const course = await CourseContents.findOne({ _id: courseId })
    const result = await course.content.find(id => id.contentTitle === classname)
    return result

}
exports.postContentMonthService = async (data) => {
    const result = await Month.create(data);
    return result;
}
exports.getContentMonthService = async (courseName) => {
    const result = await Month.find({ courseName: courseName });
    return result;
}
exports.deleteMonthService = async (id) => {
    const result = await Month.deleteOne({ _id: id });
    return result;
}

exports.getMonthByIdService = async (id) => {
    const result = await Month.findById(id);
    return result;
}
exports.updateMonthService = async (id, data) => {
    const result = await Month.updateOne({ _id: id }, { $set: data }, {
        runValidators: true
    });
    return result;
}

// subject 

exports.postContentSubjectService = async (data) => {
    const result = await Subject.create(data);
    return result;
}
exports.getContentSubjectService = async (courseName, courseMonth) => {
    const result = await Subject.find({ courseName: courseName, month: courseMonth });
    return result;
}
exports.deleteSubjectService = async (id) => {
    const result = await Subject.deleteOne({ _id: id });
    return result;
}

exports.getSubjectByIdService = async (id) => {
    const result = await Subject.findById(id);
    return result;
}
exports.updateSubjectService = async (id, data) => {
    const result = await Subject.updateOne({ _id: id }, { $set: data }, {
        runValidators: true
    });
    return result;
}

exports.getSingleContentByQueryService = async (courseName, month, subject) => {
    const result = await CourseContents.find({ courseName: courseName, month: month, subject: subject })
    return result;

}

exports.deleteContentService = async (id) => {
    const result = await CourseContents.deleteOne({ _id: id });
    return result;
}