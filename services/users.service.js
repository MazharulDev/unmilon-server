const Users = require('../models/Users')

exports.createUsersService = async (data) => {
    const user = await Users.create(data);
    return user;
}

exports.getUsersService = async () => {
    const users = await Users.find({})
    return users;
}

exports.getAdmitService = async (email) => {
    const user = await Users.findOne({ email: email });
    const isAdmin = user.role === 'admin';
    return { admin: isAdmin }
}

exports.getUserByEmailService = async (email) => {
    const query = { email: email }
    const user = await Users.find(query)
    return user;
}

exports.deleteUserByIdService = async (id) => {
    const result = await Users.deleteOne({ _id: id })
    return result;
}

exports.makeAdminByEmailService = async (userEmail) => {
    const result = await Users.updateOne({ email: userEmail }, { $set: { role: 'admin' } })
    return result;
}

exports.makeTeacherByEmailService = async (userEmail) => {
    const result = await Users.updateOne({ email: userEmail }, { $set: { role: 'teacher' } })
    return result;
}
exports.updateUserByEmailService = async (email, userData) => {
    const result = await Users.updateOne({ email: email }, { $set: userData });
    return result;
}
exports.updateUserPhotoByEmailService = async (email, userPhoto) => {
    const result = await Users.updateOne({ email: email }, { $set: userPhoto });
    return result;
}