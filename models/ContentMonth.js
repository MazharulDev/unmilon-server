const mongoose = require("mongoose")

// schema design for course
const MonthSchema = mongoose.Schema({
    courseName: {
        type: String,
        required: [true, "Please select course name"]
    },
    month: {
        type: String,
        required: [true, "Please type your month"],
    },
},
    {
        timestamps: true
    });
// schema -> Model -> query

const Month = mongoose.model('Month', MonthSchema);

module.exports = Month;