const mongoose = require("mongoose")

// schema design for course
const SubjectSchema = mongoose.Schema({
    courseName: {
        type: String,
        required: [true, "Please select course name"]
    },
    month: {
        type: String,
        required: [true, "Please select course Month"]
    },
    subject: {
        type: String,
        required: [true, "Please type your subject"]
    },
},
    {
        timestamps: true
    });
// schema -> Model -> query

const Subject = mongoose.model('Subject', SubjectSchema);

module.exports = Subject;