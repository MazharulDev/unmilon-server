const mongoose = require("mongoose")

// schema design for course
const courseContents = mongoose.Schema({
    courseName: {
        type: String,
        required: [true, "Please select course name"]
    },
    month: {
        type: String,
        required: [true, "Please select month"]
    },
    subject: {
        type: String,
        required: [true, "Please select subject"]
    },
    videoName: {
        type: String,
        required: [true, "Please type video name"]
    },
    videoLink: {
        type: String,
        required: [true, "Please type video Link"]
    },
    slideName: {
        type: String,
        required: [true, "Please type slide name"]
    },
    slideFile: {
        type: String,
    },
    pdfName: {
        type: String,
        required: [true, "Please type pdf name"]
    },
    pdfFile: {
        type: String,
    },
    quizName: {
        type: String,
        required: [true, "Please type quiz name"]
    },
    quizFile: {
        type: String,
    },


},
    {
        timestamps: true
    });
// schema -> Model -> query

const CourseContents = mongoose.model('courseContents', courseContents);

module.exports = CourseContents;