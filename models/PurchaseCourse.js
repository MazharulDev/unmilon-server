const mongoose = require("mongoose")

// schema design for user
const purchaseCourseSchema = mongoose.Schema({
    name: String,
    phoneNumber: String,
    email: String,
    currency: String,
    postcode: String,
    address: String,
    courseId: String,
    coursePrice: Number,
    courseName: String,
    transactionId: String,
    paid: Boolean,
    courseImg: String,
    courseContent: Object

},
    {
        timestamps: true
    });
// schema -> Model -> query

const PurchaseCourse = mongoose.model('PurchaseCourse', purchaseCourseSchema);

module.exports = PurchaseCourse;
