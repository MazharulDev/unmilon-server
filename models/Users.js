const mongoose = require("mongoose")

// schema design for user
const usersSchema = mongoose.Schema({
    name: {
        type: String,
        required: [true, "Please type your name"],
        minLength: [3, "Name must be 3 character"],
        maxLength: [30, "Your Name is too large"]
    },
    email: {
        type: String,
        unique: true,
        required: [true, "please type your email"]
    },
    phoneNumber: {
        type: String,
        unique: [true, "Phone number is unique"],
        required: [true, "please type you phone number"]
    },
    institute: {
        type: String,
        required: [true, "Please type your institute name"]
    },
    password: {
        type: String, Number, Boolean, Symbol,
    },
    confirmPassword: {
        type: String, Number, Boolean, Symbol,
    },
    role: {
        type: String
    },
    gender: {
        type: String
    },
    dateofbirth: {
        type: String
    },
    nationality: {
        type: String
    },
    permanentaddress: {
        type: String
    },
    presentaddress: {
        type: String
    },
    postcode: {
        type: String
    },
    religion: {
        type: String
    },
    imgURL: {
        type: String
    }

},
    {
        timestamps: true
    });
// schema -> Model -> query

const Users = mongoose.model('Users', usersSchema);

module.exports = Users;
