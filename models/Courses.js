const mongoose = require("mongoose")

// schema design for course
const coursesSchema = mongoose.Schema({
    courseCategories: {
        type: String,
        required: [true, "Please select your course name"],
        trim: true
    },
    title: {
        type: String,
        required: [true, "Please type your course title"]
    },
    offer: {
        type: String,
        required: [true, "please select offer special offer"],
        enum: ["Running", "Ended"]
    },
    courseIntro: {
        type: String,
        required: [true, "please type class video link"]
    },
    status: {
        type: String,
        required: [true, "Please select course status"]
    },
    price: {
        type: Number,
        required: true,
        min: [0, "Price can't be negative"]
    },
    img: {
        type: String,
        require: [true, "please provide the image link"]
    },
    description: {
        type: String,
    },
    // content: [
    //     {
    //         chapter: {
    //             type: String,
    //             required: [true, "Please type you chapter name"]
    //         },
    //         contentTitle: {
    //             type: String,
    //             required: [true, "Please type content title"]
    //         },
    //         link: {
    //             type: String,
    //             required: [true, "please type class video link"]
    //         },
    //         description: {
    //             type: String,
    //             required: [true, "please type class description"]
    //         }
    //     }
    // ]

},
    {
        timestamps: true
    });
// schema -> Model -> query

const Courses = mongoose.model('Courses', coursesSchema);

module.exports = Courses;
