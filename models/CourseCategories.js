const mongoose = require("mongoose")

// schema design for course
const courseCategoriesSchema = mongoose.Schema({
    courseCategories: {
        type: String,
        required: [true, "Please type your course categories"],
        unique: [true, "Course categories is unique"],
    },
},
    {
        timestamps: true
    });
// schema -> Model -> query

const CourseCategories = mongoose.model('CourseCategories', courseCategoriesSchema);

module.exports = CourseCategories;
