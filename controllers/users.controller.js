const { createUsersService, getUsersService, getAdmitService, getUserByEmailService, deleteUserByIdService, makeAdminByEmailService, makeTeacherByEmailService, updateUserByEmailService, updateUserPhotoByEmailService } = require("../services/users.service")


exports.postUser = async (req, res, next) => {
    try {
        const result = await createUsersService(req.body);

        res.status(200).json({
            status: "success",
            message: "user intert successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "user intert failed",
            error: error.message
        })
    }
}

exports.getUser = async (req, res, next) => {
    try {
        const result = await getUsersService();

        res.status(200).json({
            status: "success",
            message: "data intert successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "data intert failed",
            error: error.message
        })
    }
}

module.exports.getAdmin = async (req, res, next) => {
    try {
        const email = req.params.email;
        const result = await getAdmitService(email)

        res.status(200).json({
            status: "success",
            message: "admin load successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "Failed",
            message: "admin not Load",
            error: error.message
        })
    }
}
module.exports.getUserByEmail = async (req, res, next) => {
    try {
        const email = req.query.email;
        const result = await getUserByEmailService(email)

        res.status(200).json({
            status: "success",
            message: "user email load successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "Failed",
            message: "user email not Load",
            error: error.message
        })
    }
}

module.exports.deleteUserById = async (req, res, next) => {
    try {
        const { id } = req.params;
        const result = await deleteUserByIdService(id)

        res.status(200).json({
            status: "success",
            message: "user email load successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "Failed",
            message: "user email not Load",
            error: error.message
        })
    }
}

module.exports.makeAdminByEmail = async (req, res, next) => {
    try {
        const { email } = req.params;
        const result = await makeAdminByEmailService(email)

        res.status(200).json({
            status: "success",
            message: "Make admin successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "Failed",
            message: "make admin unsuccessfull",
            error: error.message
        })
    }
}

module.exports.makeTeacherByEmail = async (req, res, next) => {
    try {
        const { email } = req.params;
        const result = await makeTeacherByEmailService(email)

        res.status(200).json({
            status: "success",
            message: "Make admin successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "Failed",
            message: "make admin unsuccessfull",
            error: error.message
        })
    }
}
module.exports.updateUserByEmail = async (req, res, next) => {
    try {
        const { email } = req.params;
        const result = await updateUserByEmailService(email, req.body)

        res.status(200).json({
            status: "success",
            message: "update user successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "Failed",
            message: "Update user unsuccessfull",
            error: error.message
        })
    }
}
module.exports.updateUserPhotoByEmail = async (req, res, next) => {
    try {
        const { email } = req.params;
        const result = await updateUserPhotoByEmailService(email, req.body)

        res.status(200).json({
            status: "success",
            message: "update user photo successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "Failed",
            message: "Update user photo unsuccessfull",
            error: error.message
        })
    }
}

exports.userProfileImgUpload = async (req, res, next) => {
    try {
        res.status(200).json({
            status: "success",
            message: "Profile image upload successfully",
            imgURL: `${process.env.SERVER_URL}/profile-img/${req.file.filename}`
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "user profile image upload failed",
            error: error.message
        })
    }
}