const { createCourseContentService, getCourseContentService, getCourseContentByNameService, getCourseContentByIdService, getCourseContentClassByIdService, postContentMonthService, getContentMonthService, deleteMonthService, updateMonthService, getMonthByIdService, postContentSubjectService, getContentSubjectService, deleteSubjectService, getSubjectByIdService, updateSubjectService, getSingleContentByQueryService, deleteContentService } = require("../services/courseContents.service")

exports.postCourseContents = async (req, res, next) => {
    try {
        const result = await createCourseContentService(req.body);

        res.status(200).json({
            status: "success",
            message: "Course contents insert successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "Course contents insert failed",
            error: error.message
        })
    }
}

exports.getCourseContents = async (req, res, next) => {
    try {
        const result = await getCourseContentService();

        res.status(200).json({
            status: "success",
            message: "Course contents insert successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "Course contents insert failed",
            error: error.message
        })
    }
}

exports.getCourseContentsByName = async (req, res, next) => {
    try {
        const { name } = req.params;
        const result = await getCourseContentByNameService(name);

        res.status(200).json({
            status: "success",
            message: "Course contents show by name successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "Course contents shown failed",
            error: error.message
        })
    }
}
exports.getCourseContentsById = async (req, res, next) => {
    try {
        const { id } = req.params;
        const result = await getCourseContentByIdService(id);

        res.status(200).json({
            status: "success",
            message: "Course contents show by name successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "Course contents shown failed",
            error: error.message
        })
    }
}

exports.getCourseContentsClassById = async (req, res, next) => {
    try {
        // const { id } = req.params;
        const courseId = req.query.courseid;
        const classname = req.query.classname
        const result = await getCourseContentClassByIdService(courseId, classname);

        res.status(200).json({
            status: "success",
            message: "Course class show by id successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "Course class shown failed",
            error: error.message
        })
    }
}

exports.postContentMonth = async (req, res, next) => {
    try {
        const result = await postContentMonthService(req.body);

        res.status(200).json({
            status: "success",
            message: "Content Month added successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "Content month added failed",
            error: error.message
        })
    }
}
exports.getContentMonth = async (req, res, next) => {
    try {
        const courseName = req.query.coursename;
        const result = await getContentMonthService(courseName);

        res.status(200).json({
            status: "success",
            message: "Content Month shown by name successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "Content month shown by name failed",
            error: error.message
        })
    }
}
exports.deleteMonth = async (req, res, next) => {
    try {
        const { id } = req.params;
        const result = await deleteMonthService(id);

        res.status(200).json({
            status: "success",
            message: "Content Month delete successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "Content month delete failed",
            error: error.message
        })
    }
}

exports.getMonthById = async (req, res, next) => {
    try {
        const { id } = req.params;
        const result = await getMonthByIdService(id);

        res.status(200).json({
            status: "success",
            message: "Content Month delete successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "Content month delete failed",
            error: error.message
        })
    }
}
exports.updateMonth = async (req, res, next) => {
    try {
        const { id } = req.params;
        const result = await updateMonthService(id, req.body);

        res.status(200).json({
            status: "success",
            message: "Content Month update successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "Content month update failed",
            error: error.message
        })
    }
}

// subject add route

exports.postContentSubject = async (req, res, next) => {
    try {
        const result = await postContentSubjectService(req.body);

        res.status(200).json({
            status: "success",
            message: "Content subject added successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "Content subject added failed",
            error: error.message
        })
    }
}

exports.getContentSubject = async (req, res, next) => {
    try {
        const courseName = req.query.coursename;
        const courseMonth = req.query.month;
        const result = await getContentSubjectService(courseName, courseMonth);

        res.status(200).json({
            status: "success",
            message: "Content subject shown successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "Content subject shown failed",
            error: error.message
        })
    }
}
exports.deleteSubject = async (req, res, next) => {
    try {
        const { id } = req.params;
        const result = await deleteSubjectService(id);

        res.status(200).json({
            status: "success",
            message: "Content subject delete successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "Content subject delete failed",
            error: error.message
        })
    }
}

exports.getSubjectById = async (req, res, next) => {
    try {
        const { id } = req.params;
        const result = await getSubjectByIdService(id);

        res.status(200).json({
            status: "success",
            message: "Content subject show successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "Content subject get failed",
            error: error.message
        })
    }
}
exports.updateSubject = async (req, res, next) => {
    try {
        const { id } = req.params;
        const result = await updateSubjectService(id, req.body);

        res.status(200).json({
            status: "success",
            message: "Content subject update successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "Content subject update failed",
            error: error.message
        })
    }
}

exports.getSingleContentByQuery = async (req, res, next) => {
    try {
        // const { id } = req.params;
        const courseName = req.query.courseName;
        const month = req.query.month;
        const subject = req.query.subject;
        const result = await getSingleContentByQueryService(courseName, month, subject);

        res.status(200).json({
            status: "success",
            message: "single content show by query successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "single content shown failed",
            error: error.message
        })
    }
}

exports.deleteContent = async (req, res, next) => {
    try {
        const { id } = req.params;
        const result = await deleteContentService(id);

        res.status(200).json({
            status: "success",
            message: "Content delete successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "Content delete failed",
            error: error.message
        })
    }
}