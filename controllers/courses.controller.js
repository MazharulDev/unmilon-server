const { createCourseService, getCoursesService, getSpecificCourseServiec, getCourseByIdService, createCourseCategoriesService, getCourseCategoriesService, deleteCategoriesByIdService, deleteCourseByIdService, updateCategoriesByIdService, getCategoriesByIdService, postPymentByInfoService, paymentSuccessService, purchaseCourseService, paymentFailService, paymentCancelService, getMyCourseByEmailService, getCourseNameService } = require('../services/courses.service');

exports.postCourse = async (req, res, next) => {
    try {
        const result = await createCourseService(req.body);

        res.status(200).json({
            status: "success",
            message: "Course insert successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "Course insert failed",
            error: error.message
        })
    }
}

exports.getCourses = async (req, res, next) => {
    try {
        const result = await getCoursesService();

        res.status(200).json({
            status: "success",
            message: "data intert successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "get course failed",
            error: error.message
        })
    }
}

exports.getCourseById = async (req, res, next) => {
    try {
        const { id } = req.params;
        const result = await getCourseByIdService(id);

        res.status(200).json({
            status: "success",
            message: "data intert successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "get course by id failed",
            error: error.message
        })
    }
}

exports.getSpecificCourse = async (req, res, next) => {
    try {
        const { name } = req.params;
        const result = await getSpecificCourseServiec(name);

        res.status(200).json({
            status: "success",
            message: "data intert successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "data intert failed",
            error: error.message
        })
    }
}

exports.postCategories = async (req, res, next) => {
    try {
        const result = await createCourseCategoriesService(req.body);

        res.status(200).json({
            status: "success",
            message: "Course categories insert successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "Course categories insert failed",
            error: error.message
        })
    }
}

exports.getCourseCategories = async (req, res, next) => {
    try {
        const result = await getCourseCategoriesService();

        res.status(200).json({
            status: "success",
            message: "course categories show successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "course categories show failed",
            error: error.message
        })
    }
}

exports.deleteCourseCategoriesById = async (req, res, next) => {
    try {
        const { id } = req.params;
        const result = await deleteCategoriesByIdService(id);

        res.status(200).json({
            status: "success",
            message: "course categories delete successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "course categories delete failed",
            error: error.message
        })
    }
}

exports.deleteCourseById = async (req, res, next) => {
    try {
        const { id } = req.params;
        const result = await deleteCourseByIdService(id);

        res.status(200).json({
            status: "success",
            message: "course delete successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "course delete failed",
            error: error.message
        })
    }
}

exports.updateCourseCategoriesById = async (req, res, next) => {
    try {
        const { id } = req.params;
        const result = await updateCategoriesByIdService(id, req.body);

        res.status(200).json({
            status: "success",
            message: "course categories delete successfully"
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "course categories delete failed",
            error: error.message
        })
    }
}

exports.getCategoriesById = async (req, res, next) => {
    try {
        const { id } = req.params;
        const result = await getCategoriesByIdService(id);

        res.status(200).json({
            status: "success",
            message: "course categories intert successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "course categories intert failed",
            error: error.message
        })
    }
}

// file upload controller 

exports.courseImgUpload = async (req, res, next) => {
    try {
        res.status(200).json({
            status: "success",
            message: "Course thumbonail upload successfully",
            imgURL: `${process.env.SERVER_URL}/course-thum/${req.file.filename}`
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "Course thumbnail upload failed",
            error: error.message
        })
    }
}

exports.courseVideo = async (req, res, next) => {
    try {
        res.status(200).json({
            status: "success",
            message: "Course video upload successfully",
            videoURL: `${process.env.SERVER_URL}/course-video/${req.file.filename}`
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "Course video upload failed",
            error: error.message
        })
    }
}

exports.coursePdf = async (req, res, next) => {
    try {
        res.status(200).json({
            status: "success",
            message: "Course pdf upload successfully",
            pdfURL: `${process.env.SERVER_URL}/course-pdf/${req.file.filename}`
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "Course pdf upload failed",
            error: error.message
        })
    }
}

exports.quizUpload = async (req, res, next) => {
    try {
        res.status(200).json({
            status: "success",
            message: "quiz file upload successfully",
            quizURL: `${process.env.SERVER_URL}/quiz/${req.file.filename}`
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "Course pdf upload failed",
            error: error.message
        })
    }
}

exports.postPymentByInfo = async (req, res, next) => {
    try {
        const data = req.body;
        const { name, email, address, postcode, phoneNumber } = data;
        if (!name || !email || !address || !postcode || !phoneNumber) {
            return res.send({ error: "please provide all the information" });
        }
        const result = await postPymentByInfoService(data)

        res.status(200).json({
            status: "success",
            message: "payment processing",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "payment failed",
            error: error.message
        })
    }
}
exports.paymentSuccess = async (req, res, next) => {
    try {
        const { transactionId } = req.query;
        if (!transactionId) {
            res.redirect(`${process.env.CLIENT_URL}/payment/fail`)
        }
        const result = await paymentSuccessService(transactionId)
        if (result.modifiedCount > 0) {
            res.redirect(`${process.env.CLIENT_URL}/payment/success?transactionId=${transactionId}`)
        }
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "payment failed",
            error: error.message
        })
    }
}

exports.paymentFail = async (req, res, next) => {
    try {
        const { transactionId } = req.query;
        if (!transactionId) {
            res.redirect(`${process.env.CLIENT_URL}/payment/fail`)
        }
        const result = await paymentFailService(transactionId)
        if (result.deletedCount) {
            res.redirect(`${process.env.CLIENT_URL}/payment/fail`)
        }
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "payment failed",
            error: error.message
        })
    }
}

exports.paymentCancel = async (req, res, next) => {
    try {
        const { transactionId } = req.query;
        if (!transactionId) {
            res.redirect(`${process.env.CLIENT_URL}/payment/fail`)
        }
        const result = await paymentCancelService(transactionId)
        if (result.deletedCount) {
            res.redirect(`${process.env.CLIENT_URL}/payment/fail`)
        }

    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "payment cancel failed",
            error: error.message
        })
    }
}

exports.purchaseCourse = async (req, res, next) => {
    try {
        const { id } = req.params;
        const result = await purchaseCourseService(id)

        res.status(200).json({
            status: "success",
            message: "Purchase course data get success",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "Purchase course data get failed",
            error: error.message
        })
    }
}

exports.getMyCourseByEmail = async (req, res, next) => {
    try {
        const { email } = req.params;
        const result = await getMyCourseByEmailService(email);

        res.status(200).json({
            status: "success",
            message: "My Course show successfully",
            data: result
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: "My course show failed",
            error: error.message
        })
    }
}
