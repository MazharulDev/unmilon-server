const mongoose = require("mongoose")
require("dotenv").config();
const colors = require("colors");

const app = require("./app");

//database conntection
mongoose.set("strictQuery", false);
mongoose.connect(process.env.DATABASE).then(() => {
    console.log("Database connection successfull".blue.bold);
});

// server 
const port = process.env.PORT || 8080;

app.listen(port, () => {
    console.log(`app is running port ${port}`.yellow.bold);
});