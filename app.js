const express = require("express");
const app = express();
const cors = require("cors");
const mongoose = require("mongoose");

// middleware
app.use(express.json());
app.use(cors());

// routes
const usersRoute = require("./routes/users.route")
const courseRoute = require("./routes/courses.route")
const courseContentsRoute = require("./routes/courseContents.route")

app.get('/', (req, res) => {
    res.send("Home route is working")
});


// database route
app.use('/api/v1/users', usersRoute)
app.use('/api/v1/courses', courseRoute)
app.use('/api/v1/course-content', courseContentsRoute)

// file access route
app.use("/course-thum", express.static("./assets/coursethum"))
app.use("/course-video", express.static("./assets/courseVideo"))
app.use("/course-pdf", express.static("./assets/coursePdf"))
app.use('/profile-img', express.static("./assets/userProfileImg"))
app.use('/quiz', express.static("./assets/quizfile"))

// default error handling 
app.use((err, req, res, next) => {
    if (err) {
        res.status(500).json({
            status: "failed",
            error: err.message
        })
    } else {
        res.send("success")
    }
});
module.exports = app;