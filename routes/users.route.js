const express = require("express");
const router = express.Router();
const usersController = require("../controllers/users.controller");
const userImg = require("../middleware/userProfileImgUpload");

router.route('/')
    .post(usersController.postUser)
    .get(usersController.getUser)
router.route("/:id")
    .delete(usersController.deleteUserById)
router.route('/admin/:email')
    .get(usersController.getAdmin)
router.route("/makeadmin/:email")
    .put(usersController.makeAdminByEmail)
router.route("/maketeacher/:email")
    .put(usersController.makeTeacherByEmail)
router.route('/userprofile')
    .get(usersController.getUserByEmail)
router.route('/updateuser/:email')
    .put(usersController.updateUserByEmail)
router.route('/updateuser-photo/:email')
    .put(usersController.updateUserPhotoByEmail)

router.route("/profile-img")
    .post(userImg.single('userImg'), usersController.userProfileImgUpload)

module.exports = router;