const express = require("express");
const router = express.Router();
const courseContentsController = require('../controllers/courseContents.controller')

router.route("/")
    .post(courseContentsController.postCourseContents)
    .get(courseContentsController.getCourseContents)
router.route('/name/:name')
    .get(courseContentsController.getCourseContentsByName)
router.route("/content/:id")
    .get(courseContentsController.getCourseContentsById)
router.route("/contents/class")
    .get(courseContentsController.getCourseContentsClassById)


router.route("/month")
    .post(courseContentsController.postContentMonth)
    .get(courseContentsController.getContentMonth)
router.route("/month/:id")
    .delete(courseContentsController.deleteMonth)
    .get(courseContentsController.getMonthById)
    .patch(courseContentsController.updateMonth)

router.route("/subject")
    .post(courseContentsController.postContentSubject)
    .get(courseContentsController.getContentSubject)
router.route("/subject/:id")
    .delete(courseContentsController.deleteSubject)
    .get(courseContentsController.getSubjectById)
    .patch(courseContentsController.updateSubject)

router.route("/single-content")
    .get(courseContentsController.getSingleContentByQuery)
router.route("/content-delete/:id")
    .delete(courseContentsController.deleteContent)
module.exports = router;