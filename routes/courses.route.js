const express = require("express");
const router = express.Router();
const coursesController = require("../controllers/courses.controller");
const courseVideoUpload = require("../middleware/courseVideoUpload");
const courseThumUpload = require("../middleware/courseThumUpload");
const coursePdfUpload = require("../middleware/coursePdfUpload");
const quizFileUpload = require("../middleware/quizFileUpload");


router.route("/")
    .post(coursesController.postCourse)
    .get(coursesController.getCourses)
router.route("/:id")
    .delete(coursesController.deleteCourseById)
router.route("/specific/:name")
    .get(coursesController.getSpecificCourse)
router.route("/categoriesAdd")
    .post(coursesController.postCategories)
    .get(coursesController.getCourseCategories)
router.route("/catagoriesUpdate/:id")
    .patch(coursesController.updateCourseCategoriesById)
router.route("/getCategoriesById/:id")
    .get(coursesController.getCategoriesById)
router.route("/categoriesdelete/:id")
    .delete(coursesController.deleteCourseCategoriesById)
router.route("/:id")
    .get(coursesController.getCourseById)
router.route('/payment')
    .post(coursesController.postPymentByInfo)
router.route("/payment/success")
    .post(coursesController.paymentSuccess)
router.route("/payment/fail")
    .post(coursesController.paymentFail)
router.route("/payment/cancel")
    .post(coursesController.paymentCancel)
router.route("/purchase-course/:id")
    .get(coursesController.purchaseCourse)
router.route("/my-course/:email")
    .get(coursesController.getMyCourseByEmail)

// course thumbnail upload route
router.route("/coursethum")
    .post(courseThumUpload.single('courseImg'), coursesController.courseImgUpload)
// course video upload 
router.route("/coursevideo")
    .post(courseVideoUpload.single('courseVideo'), coursesController.courseVideo)
// course pdf upload
router.route("/coursepdf")
    .post(coursePdfUpload.single("coursePdf"), coursesController.coursePdf)
// quiz file upload 
router.route("/quizfile")
    .post(quizFileUpload.single("quiz"), coursesController.quizUpload)
module.exports = router;