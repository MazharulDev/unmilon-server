const multer = require("multer");
const path = require("path");

const storage = multer.diskStorage({
    destination: "assets/coursePdf/",
    filename: (req, file, cb) => {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
        cb(null, uniqueSuffix + "-" + file.originalname)
    }
})

const coursePdfUpload = multer({
    storage,
    fileFilter: (req, file, cb) => {
        const supportedFile = /pdf/;
        const extenstion = path.extname(file.originalname);

        if (supportedFile.test(extenstion)) {
            cb(null, true);
        } else {
            cb(new Error("Course file must be a pdf added"))
        }
    },
    limits: {
        fileSize: 3000000
    }
})

module.exports = coursePdfUpload;