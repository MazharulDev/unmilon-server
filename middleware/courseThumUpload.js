const multer = require("multer");
const path = require("path");

const storage = multer.diskStorage({
    destination: "assets/coursethum/",
    filename: (req, file, cb) => {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
        cb(null, uniqueSuffix + "-" + file.originalname)
    }
})

const courseImgUploader = multer({
    storage,
    fileFilter: (req, file, cb) => {
        const supportedImg = /png|jpg/;
        const extenstion = path.extname(file.originalname);

        if (supportedImg.test(extenstion)) {
            cb(null, true);
        } else {
            cb(new Error("Course thumbnail must be a png or jpg added"))
        }
    },
    limits: {
        fileSize: 3000000
    }
})

module.exports = courseImgUploader;