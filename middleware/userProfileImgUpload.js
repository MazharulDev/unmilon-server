const multer = require("multer");
const path = require("path");

const storage = multer.diskStorage({
    destination: "assets/userProfileImg/",
    filename: (req, file, cb) => {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
        cb(null, uniqueSuffix + "-" + file.originalname)
    }
})

const userImg = multer({
    storage,
    fileFilter: (req, file, cb) => {
        const supportedImg = /jpg|png/;
        const extenstion = path.extname(file.originalname);

        if (supportedImg.test(extenstion)) {
            cb(null, true);
        } else {
            cb(new Error("User image must be a jpg or png added"))
        }
    },
    limits: {
        fileSize: 3000000
    }
})

module.exports = userImg;