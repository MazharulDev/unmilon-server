const multer = require("multer");
const path = require("path");

const storage = multer.diskStorage({
    destination: "assets/quizfile/",
    filename: (req, file, cb) => {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
        cb(null, uniqueSuffix + "-" + file.originalname)
    }
})

const quizFileUpload = multer({
    storage,
    fileFilter: (req, file, cb) => {
        const supportedFile = /json/;
        const extenstion = path.extname(file.originalname);

        if (supportedFile.test(extenstion)) {
            cb(null, true);
        } else {
            cb(new Error("Quiz file must be a json added"))
        }
    },
    limits: {
        fileSize: 1000000
    }
})

module.exports = quizFileUpload;