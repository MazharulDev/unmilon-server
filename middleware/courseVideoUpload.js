const multer = require("multer");
const path = require("path");

const storage = multer.diskStorage({
    destination: "assets/courseVideo/",
    filename: (req, file, cb) => {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
        cb(null, uniqueSuffix + "-" + file.originalname)
    }
})

const courseVideo = multer({
    storage,
    fileFilter: (req, file, cb) => {
        const supportedVideo = /mp4|mkv/;
        const extenstion = path.extname(file.originalname);

        if (supportedVideo.test(extenstion)) {
            cb(null, true);
        } else {
            cb(new Error("Course video must be a mp4 or mkv added"))
        }
    },
    limits: {
        fileSize: 50000000
    }
})

module.exports = courseVideo;